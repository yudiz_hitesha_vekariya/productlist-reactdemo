import React from "react";

export default function Header(props) {
  return (
    <header className="block row center">
      <div>
        <a href="#/">
          <h1>Small Shopping Cart</h1>
        </a>
      </div>
      <div className="search">
        <input type="text" placeholder="search the products here..." />
      </div>
      <div>
        <a href="#/cart">
          <i className="fas fa-shopping-cart" />
          {props.countCartItems ? (
            <button className="badge">{props.countCartItems}</button>
          ) : (
            ""
          )}
        </a>{" "}
        <a href="#/signin"> SignIn</a>
      </div>
    </header>
  );
}
