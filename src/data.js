const data = {
  products: [
    {
      id: "1",
      name: "MacBook",
      price: 1400,
      image: "https://picsum.photos/id/180/2400/1600",
    },
    {
      id: "2",
      name: "onepiece w",
      price: 1408,
      image:
        "https://rukminim1.flixcart.com/image/332/398/kflftzk0/dress/n/c/d/l-one-piece-orrange-line-maa-fab-original-imafwyy3esacjzau.jpeg?q=50",
    },
    {
      id: "3",
      name: "Old Car",
      price: 2400,
      image: "https://picsum.photos/id/111/4400/2656",
    },
    {
      id: "4",
      name: "W Shoes",
      price: 1000,
      image:
        "https://st2.depositphotos.com/1016929/7816/i/600/depositphotos_78165392-stock-photo-red-bow-wedges.jpg",
    },
    {
      id: "5",
      name: "Old Car",
      price: 2500,
      image:
        "https://rukminim1.flixcart.com/image/416/416/jk1grrk0/poster/y/t/s/medium-fps251-beautiful-old-car-vintage-car-old-classic-car-original-imaf7gsrr4fgfgza.jpeg?q=70",
    },
    {
      id: "6",
      name: "W Shoes",
      price: 240,
      image:
        "https://image.made-in-china.com/202f0j00DkqUSacdrjbo/Amazon-New-Arrival-Stylish-Women-Shoes-Ladies-Casual-Sneaker-Sports-Shoes-for-Women.jpg",
    },
    {
      id: "7",
      name: "Old Car",
      price: 3400,
      image:
        "http://www.autoini.com/wp-content/uploads/2020/04/autoini-be-alert-while-buying-an-old-car.jpg",
    },
    {
      id: "8",
      name: "W Shoes",
      price: 247,
      image:
        "https://media1.popsugar-assets.com/files/thumbor/cf4ULndaPs0BK4gfefHPw2eRESA/0x347:1550x2402/fit-in/728xorig/filters:format_auto-!!-:strip_icc-!!-/2020/01/24/947/n/1922564/7eb488485e2b655e6bd637.07827357_/i/Best-Shoes-Women-2020.jpg",
    },
    {
      id: "9",
      name: "onepiece w",
      price: 2408,
      image:
        "https://assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/13719154/2021/3/5/3fda7627-9073-4702-a66e-f32f10bc64021614925061807-Athena-Fuchsia-Pink-dress-with-Balloon-puff-sleeves-81614925-1.jpg",
    },
    {
      id: "10",
      name: "makeup kit",
      price: 1100,
      image: "https://wwd.com/wp-content/uploads/2021/12/best-makeup-kits.jpg",
    },
  ],
};
export default data;
